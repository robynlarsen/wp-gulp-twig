/*
 * Process application styles.
 */

var paths = require('./paths.js')
var gulp = require('gulp')
var stylus = require('gulp-stylus')
var autoprefixer = require('gulp-autoprefixer')
var notify = require('gulp-notify')
var sourcemaps = require('gulp-sourcemaps')
var filesize = require('gulp-size')

gulp.task('styles', function(){
  return gulp.src(paths.styles.src)

    .pipe(sourcemaps.init())

    .pipe(stylus({
      cache: false,
      compress: true,
      'include css': true
    }).on('error', notify.onError({
      title: 'Stylus Error',
      message: "\n<%= error.message %>"
    })))

    .pipe(autoprefixer())

    .pipe(filesize({showFiles: true}))

    .pipe(sourcemaps.write('.'))

    .pipe(gulp.dest(paths.styles.dest))

})
