var destRoot = '../wp-content/themes/healthAnalytics'

module.exports = {
  root: destRoot,
  styles: {
    watch: './assets/styles/**/*.*',
    src: './assets/styles/style.styl',
    dest: destRoot
  },
  scripts: {
    watch: './assets/scripts/**/*.*',
    src: './assets/scripts/**/*.js',
    dest: destRoot + '/js'
  },
  views: {
    watch: './assets/views/**/*.*',
    src: './assets/views/**/*.twig',
    dest: destRoot + '/templates'
  },
  php: {
    src: ['./assets/php/**/*.php', './assets/php/**/*.xml'],
    dest: destRoot
  },
  fonts: {
    src: './assets/fonts/**/*.*',
    dest: destRoot + '/fonts'
  },
  screenshot: {
    src: './assets/images/screenshot.png',
    dest: destRoot
  },
  images: {
    src: ['./assets/images/**/*.*', '!./assets/images/**/*.svg'],
    dest: destRoot + '/images'
  },
  svgs: {
    src: './assets/images/**/*.svg',
    dest: destRoot + '/images'
  }
}
