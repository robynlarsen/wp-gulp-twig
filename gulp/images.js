/*
 * Minify images and add screenshot to root folder.
 */

var paths = require('./paths.js')
var gulp = require('gulp')
var imagemin = require('gulp-imagemin')
var pngquant = require('imagemin-pngquant')

gulp.task('screenshot', function(){
 return gulp.src(paths.screenshot.src)
   .pipe(gulp.dest(paths.screenshot.dest))
})

gulp.task('images', function(){
 return gulp.src(paths.images.src)
   .pipe(imagemin({
       progressive: true,
       svgoPlugins: [{removeViewBox: false}],
       use: [pngquant()]
     }))
     .pipe(gulp.dest(paths.images.dest))
   gulp.src(paths.svgs.src)
   .pipe(gulp.dest(paths.svgs.dest))
})