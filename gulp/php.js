/*
 * Add php files to root folder
 */

var paths = require('./paths.js')
var gulp = require('gulp')

gulp.task('php', function(){
  return gulp.src(paths.php.src)
  .pipe(gulp.dest(paths.php.dest))
})