/*
 * Copy fonts to dest.
 */

var paths = require('./paths.js')
var gulp = require('gulp')

gulp.task('fonts', function(){
  return gulp.src(paths.fonts.src)
    .pipe(gulp.dest(paths.fonts.dest))
})