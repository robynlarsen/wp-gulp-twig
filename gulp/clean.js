/*
 * Destorys the destination folder before re-populating it.
 * Clean will not run while Gulp is watching.
 */

var paths = require('./paths.js')
var gulp = require('gulp')
var del = require('del')

gulp.task('clean', function(){
  return del(paths.root, {force: true})
})