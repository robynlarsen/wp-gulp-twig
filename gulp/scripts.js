/*
 * Concat and uglify the scripts.
 */

var paths = require('./paths.js')
var gulp = require('gulp')
var uglify = require('gulp-uglify')
var concat = require('gulp-concat')
var notify = require('gulp-notify')
var filesize = require('gulp-size')

gulp.task('scripts', function(){
  return gulp.src(paths.scripts.src)

    .pipe(concat({path: 'scripts.js', cwd: ''}))

    .pipe(uglify().on('error', notify.onError({
      title: 'Uglify Error',
      message: "\n<%= error.message %>"
    })))

    .pipe(filesize({showFiles: true}))

    .pipe(gulp.dest(paths.scripts.dest))
})
