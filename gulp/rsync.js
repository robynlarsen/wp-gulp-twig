var gulp = require('gulp')
var rsync = require('gulp-rsync')
var paths = require('./paths.js')

var host = host,
    user = user

gulp.task('rsync', function () {
  return gulp.src( paths.destRoot + '/**/*.*')
    .pipe(rsync({
      root: '../',
      hostname: host,
      username: user,
      port: 18765,
      destination: './public_html/gsoccer/',
      incremental: true,
      progress: true,
      clean: true,
      recursive: true,
      update: true,
      exclude: 'style.css.map',
      emptyDirectories: true,
      force: true
    }))
})