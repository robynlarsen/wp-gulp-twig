/*
 *  Reload the browser with BrowserSync and watch files
 */

var gulp = require('gulp')
var browserSync = require('browser-sync')
var runSequence = require('run-sequence')
var paths = require('./paths.js')

// reload all connected browsers
gulp.task('reload', function() {
  browserSync.reload()
})

gulp.task('watch', function() {
  // only watch compiled css file
  // and INJECT when compiled
  // otherwise, HTML/JS changes should trigger full refresh
  browserSync.init({
    proxy: 'localhost:8888',
    files: paths.styles.dest + 'style.css',
    injectChanges: true,
    logPrefix: 'BrowserSync',
    logConnections: true
  })

  gulp.watch(paths.styles.watch).on('change',  function() {
    runSequence('styles')
  })

  gulp.watch(paths.scripts.watch).on('change',  function() {
    runSequence('scripts', 'reload')
  })

  gulp.watch(paths.views.watch).on('change',  function() {
    runSequence('views', 'reload')
  })

  gulp.watch(paths.php.src).on('change',  function() {
    runSequence('php', 'reload')
  })

  gulp.watch(paths.images.src).on('change',  function() {
    runSequence('images', 'reload')
  })
})