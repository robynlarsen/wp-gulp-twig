/*
 * Build the jade views.
 * Pipe index.html into dir root, others go in 'views' dir
 */

var paths   = require('./paths.js')
var gulp    = require('gulp')
var notify  = require('gulp-notify')
var runSequence = require('run-sequence')
var rename  = require('gulp-rename')

gulp.task('views', function(){
  return gulp.src(paths.views.src)
    .pipe(gulp.dest(paths.views.dest))
})