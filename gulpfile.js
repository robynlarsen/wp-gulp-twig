var gulp = require('gulp')
var requireDir = require('require-dir')
requireDir('./gulp')
var runSequence = require('run-sequence').use(gulp)


gulp.task('build', function(cb) {
  runSequence('clean', ['styles', 'scripts', 'php', 'views', 'fonts', 'screenshot', 'images'], cb)
})

gulp.task('default', function(cb) {
  runSequence('build', 'watch', cb)
})

// Push up only wp-contents folder
gulp.task('deploy', function(cb){
  runSequence('build', 'rsync', cb)
})