# Starter Theme
Wordpress using Twig, Jade, and Stylus.

## Installation

### WP CLI

1. Install `wp cli` - http://wp-cli.org/

### Starting WordPress from Scratch

1. Create project directory
2. `cd projectDirectory`
1. `wp core download`
8. `wp core config --dbname=projectname --dbuser=root --dbpass=root`
//TODO add --dbhost=serverIP with remote
9. Creates the database in MAMP
`wp db create`
3. Navigation to localhost in browser and setup Wordpress.
2. cd into your root directory of wordpress
3. `git clone git@bitbucket.org:robynlarsen/wp-gulp-twig.git src`
4. `cd src`
5. rm -r .git
6. Move plugins folder to wp-content/plugins
7. `cd ..` Move back into root directory of wordpress
20. Activate core plugins
`wp plugin active --all`
11. `cd themename/`
12. `npm install`
15. Go to path.js --> change 'themename' to what you want the themename to be created
14. `gulp`
15. Navigation to your mamp localhost folder
ex. - `http://localhost:8888/projectfolder/themename/`
16. Start installation go through steps

### Installing Packing
Just right in!!

###
Plugins: ACF, ACF Flexible Content, ACF Gallery, ACF Options Page, ACF Repeater, Custom Post Types UI, Timber

### Deployment

This should deploy the contents of your custom theme folder. 

1. Set up host name and username within `rsync.js`
2. `gulp deploy`