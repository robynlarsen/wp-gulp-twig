(function mobile () {
  var trigger = document.getElementById('navigation-toggle')
  var target = document.getElementById('navigation-menu')

  if (trigger != undefined) {
    trigger.addEventListener('click', function () {
      target.classList.toggle('menu-open')
    })
  }

  if (window.innerWidth < 1040) {
    var menuWithChildren = document.getElementsByClassName('menu-item-has-children')
    // convert array like to array
    menuWithChildren = Array.prototype.slice.call(menuWithChildren)

    menuWithChildren.forEach(function(menuItem) {
      menuItem.addEventListener('click', function(){
        var subMenu = this.getElementsByClassName('sub-menu')[0]
        subMenu.classList.toggle('submenu-open');
      })
    })
  }
})();